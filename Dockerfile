FROM python:3.8-slim

RUN groupadd -r app -g 1000 && useradd -u 1000 -r -g app -m -d /app -s /sbin/nologin -c "App user" app && \
    chmod 755 /app

COPY ./app /app/

WORKDIR /app

USER 1000

RUN pip install --user -r requirements.txt

ENTRYPOINT ["python", "external-dns-sourcefile-generator.py"]
