# external-dns SourceFile generator

This script will collect domain+IPs pairs from various sources, and then feed a file or a Kubernetes' ConfigMap to be the source for external-dns' source plugin SourceFile.

![logo](./images/zen.jpg)

## Quick start

Just do the following:

  - run `pip install -r requirements.txt`
  - set the input and output plugins in `config/external-dns-sourcefile-generator.conf`
  - set values in each specific config file under `config/`
  - run the script (can be run byr hand from command line, using cron, etc)

## Pluggeable

This script was designed using plugins, so it already has a few input and output plugins, but easily you can add more. e.g. if you have a new domain+IPs source and need to read from this.

If you need to write a plugin, please read Collaborate.

So far, these are the plugins it has:

| Input Plugin  | Description   |
| ------------- |:-------------:|
| file          | Reads from a set of files located under a set of directories. Can match using a RegEx |
| proxmoxve     | Reads from ProxMox data about LXCs (QEMU VMs is under progress) |
| zerotiermembers | Reads from a ZeroTier Network a list of members and their IPs |


| Output Plugin  | Description   |
| -------------  |:-------------:|
| file           | Writes a json file that can be feed directly into external-dns' plugin |
| k8scm          | Writes directly the data into a ConfiMap, or various ConfigMaps (all with the same name but living in different namespaces) |
| stdout         | Writes data to stdout. Just for tests. |


