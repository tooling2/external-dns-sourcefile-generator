#!/usr/bin/env python

# external-dns SourceFile input generator
# This script will read from a given input and create the input file for external-dns' source SourceFile
# Author: Juan Matias KungFu de la Camara Beovide <juanmatias@gmail.com>
# Date: 2020-03-30

import logging
import vendors.ConfigParser as ConfigParser
import urllib.request
import json 
import os
import datetime
import ssl
import re

# This is the reader plugin for Proxmox VE Containers and VMs
# 
# Sample connection:
# #### get credentials
# curl -k -d "username=root@pam&password=yourpass"  https://192.168.69.99:8006/api2/json/access/ticket
# Result is basically this:
# { "data": { 
#  "CSRFPreventionToken":"4EEC61E2:lwk7od06fa1+DcPUwBTXCcndyAY",  
#  "ticket":"PVE:root@pam:4EEC61E2::rsKoApxDTLYPn6H3NNT6iP2mv...", 
#  "username":"root@pam"}
# }

# You need to pass the returned ticket with a cookie to any further request:
# curl -k -b "PVEAuthCookie=PVE:root@pam:4EEC61E2::rsKoApxDTLYPn6H3NNT6iP2mv..." https://192.168.69.99:8006/api2/json
#
# NOTE: Tickets have a limited lifetime of 2 hours. But you can simple get a new ticket by passing the old ticket as password to the /access/ticket method: 
# curl -k --data "username=root" --data "realm=pam" --data-urlencode "password=PVE:root@pam:5E835460::T14OpChCYNa41..."  https://192.168.69.99:8006/api2/json/access/ticket 

# Then get LXCs:


class proxmoxve_reader:
    URL = {
        "BaseUrl": "",
        "BaseUri": '/api2/json',
        "ticket": '/access/ticket',
        "nodes": '/nodes',
        "lxc": '/nodes/<node>/lxc',
        "lxcconfig": '/nodes/<node>/lxc/<lxcid>/config',
        "qemu": '/nodes/<node>/qemu',
        "qemuconfig": '/nodes/<node>/qemu/<qemuid>/config',
    }
    Insecure = False
    ticket = ""
    ticket_time = None
    # ttl in seconds less ten % so we can renew after it expires
    ticket_ttl = int(60 * 60 * 2 * 0.9)

    def __init__(self):
        logging.debug("Reading config file for input_proxmoxve module")
        try:
            config_file = os.path.dirname(os.path.realpath(__file__)) + '/../config/input_proxmoxve.conf'
            self.configParser = ConfigParser.RawConfigParser()
            self.configParser.read(config_file)
            self.PassMethod = self.configParser.get('Config', 'PassMethod')
            self.User = self.configParser.get('Config', 'User')
            if self.PassMethod == 'Plain':
                self.Pass = self.configParser.get('Config', 'Pass')
            else:
                self.Pass = ""
            try:
                self.URL["BaseUrl"] = self.configParser.get('Config', 'BaseUrl')
            except:
                pass
            try:
                self.URL["BaseUri"] = self.configParser.get('Config', 'BaseUri')
            except:
                pass
            try:
                self.Insecure = bool(self.configParser.get('urllib', 'Insecure'))
            except:
                pass

        except Exception as error:
            raise ValueError('Error reading config file ' + config_file+' ('+repr(error)+')')
        self.check_ticket()

    def proxmoxve_read(self, data):
        # Actions to take to read data
        logging.debug('Read function')

        # Read Data
        # Get nodes
        ctx=self.get_context()
        hdr = { 'Cookie': 'PVEAuthCookie=' + self.ticket}
        url = self.URL['BaseUrl']+self.URL['BaseUri']+self.URL['nodes']
        req = urllib.request.Request(url, headers=hdr)
        logging.debug('Getting nodes. Hitting url {}'.format(url))
        with urllib.request.urlopen(req, context=ctx) as url:
            resps = json.loads(url.read().decode())
        if 'data' in resps:
            for resp in resps['data']:
                logging.debug('Node found: {}'.format(resp['node']))

                # Get LXCs
                logging.debug('Looking for LXCs')
                url = self.URL['BaseUrl']+self.URL['BaseUri']+self.URL['lxc']
                url = url.replace('<node>', resp['node'])
                req = urllib.request.Request(url, headers=hdr)
                with urllib.request.urlopen(req, context=ctx) as url:
                    respslxc = json.loads(url.read().decode())
                if 'data' in respslxc:
                    for resplxc in respslxc['data']:
                        logging.debug('LXC found: {}'.format(resplxc['vmid']))
                        if resplxc['status'] == 'running':
                            logging.debug('Instance is running, adding to data.')
                            url = self.URL['BaseUrl']+self.URL['BaseUri']+self.URL['lxcconfig']
                            url = url.replace('<node>', resp['node'])
                            url = url.replace('<lxcid>', resplxc['vmid'])
                            req = urllib.request.Request(url, headers=hdr)
                            with urllib.request.urlopen(req, context=ctx) as url:
                                respslxcc = json.loads(url.read().decode())
                            if 'data' in respslxcc:
                                hostname = respslxcc['data']['hostname']
                                ip = respslxcc['data']['net0']
                                ipextract = re.compile('^.+ip=([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+).+$')
                                ipresult = ipextract.search(ip)
                                ip = ipresult.group(1)
                                data.append({'dnsname': hostname, 'addresses': [ip]})
                        else:
                            logging.debug('Instance is not running({}), skipping.'.format(resplxc['status']))

                # # Get QEMUs
                # logging.debug('Looking for QEMUs')
                # url = self.URL['BaseUrl']+self.URL['BaseUri']+self.URL['qemu']
                # url = url.replace('<node>', resp['node'])
                # req = urllib.request.Request(url, headers=hdr)
                # with urllib.request.urlopen(req, context=ctx) as url:
                #     respsqemu = json.loads(url.read().decode())
                # if 'data' in respsqemu:
                #     for resplxc in respsqemu['data']:
                #         logging.debug('QEMU found: {}'.format(respqemu['vmid']))
                #         if respqemu['status'] == 'running':
                #             logging.debug('Instance is running, adding to data.')
                #             url = self.URL['BaseUrl']+self.URL['BaseUri']+self.URL['qemuconfig']
                #             url = url.replace('<node>', resp['node'])
                #             url = url.replace('<qemuid>', respqemu['vmid'])
                #             req = urllib.request.Request(url, headers=hdr)
                #             with urllib.request.urlopen(req, context=ctx) as url:
                #                 respsqemuc = json.loads(url.read().decode())
                #             if 'data' in respsqemuc:
                #                 hostname = respsqemuc['data']['hostname']
                #                 ip = respsqemuc['data']['net0']
                #                 ipextract = re.compile('^.+ip=([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+).+$')
                #                 ipresult = ipextract.search(ip)
                #                 ip = ipresult.group(1)
                #                 data.append({'domain': hostname, 'addresses': [ip]})
                #         else:
                #             logging.debug('Instance is not running({}), skipping.'.format(respqemu['status']))
                

    def proxmoxve_shutdown(self):
        # Actions to take on process shutdown
        return True

    def proxmoxve_ack(self):
        # Actions to take when sent data ack is received
        return True

    def check_ticket(self):
        if self.ticket == "" or self.ticket_time is None:
            return self.get_ticket()
        else:
            if self.ticket_time is None:
                return self.get_ticket()
            else:
                delta = datetime.datetime.now() - self.ticket_time
                if delta.total_seconds() > self.ticket_ttl:
                    return self.get_ticket()
        return True

    def get_context(self):
        logging.debug('Setting context for urllib')
        ctx = None
        if self.Insecure:
            logging.debug('Using insecure mode for urllib')
            ctx = ssl.create_default_context()
            ctx.check_hostname = False
            ctx.verify_mode = ssl.CERT_NONE
        return ctx

    def get_ticket(self):
        logging.debug('Asking for a ticket')

        url = self.URL["BaseUrl"]+self.URL["BaseUri"]+self.URL["ticket"]
        if self.ticket == "":
            logging.debug('No previous ticket, asking for a new one.')
            my_data = 'username='+self.User+'&password='+self.Pass
        else:
            logging.debug('Previous ticket exists, asking for a renewal.')
            my_data = 'username='+self.User+'&password='+self.ticket

        resps = ""
        logging.debug('Hitting url {}'.format(url))
        with urllib.request.urlopen(url, data=bytes(my_data, encoding='utf8'), context=self.get_context()) as url:
            resps = json.loads(url.read().decode())
        if 'data' in resps and resps["data"] is not None:
            if 'ticket' in resps['data']:
                self.ticket = resps['data']['ticket']
                self.ticket_time = datetime.datetime.now()
            else:
                logging.error('No ticket got from data at ProxmoxVE')
                return False
        else:
            logging.error('No ticket got from ProxmoxVE')
            return False

        return True


if __name__ == "__main__":
    quit()

