#!/usr/bin/env python

# external-dns SourceFile input generator
# This script will read from a given input and create the input file for external-dns' source SourceFile
# Author: Juan Matias KungFu de la Camara Beovide <juanmatias@gmail.com>
# Date: 2020-03-30

import logging
import json
import vendors.ConfigParser as ConfigParser
import os
from kubernetes import client, config
from kubernetes.client.rest import ApiException

# This is the writer to a ConfigMap in Kubernetes


class k8scm_writer:
    def __init__(self):
        logging.debug('Plugin Output k8scm')
        try:
            config_file = os.path.dirname(os.path.realpath(
                __file__)) + '/../config/output_k8scm.conf'
            self.configParser = ConfigParser.RawConfigParser()
            self.configParser.read(config_file)
            self.EnvVars = bool(self.configParser.get('Kubernetes', 'EnvVars'))
            self.UseCurrentNS = self.configParser.get('Kubernetes', 'UseCurrentNS')
            self.Namespaces= self.configParser.get('Kubernetes', 'Namespaces')
            self.ConfigMapName = self.configParser.get('Kubernetes', 'ConfigMapName')
            self.KeyName = self.configParser.get('Kubernetes', 'KeyName')
            self.InCluster = bool(self.configParser.get('Config', 'InCluster'))
            self.Kubeconfig = self.configParser.get('Config', 'Kubeconfig') or os.getenv('KUBECONFIG')
            if self.EnvVars:
                if 'NAMESPACES' in os.environ:
                    self.Namespaces = os.environ['NAMESPACES']
                if 'CONFIGMAPNAME' in os.environ:
                    self.ConfigMapName = os.environ['CONFIGMAPNAME']
                if 'KEYNAME' in os.environ:
                    self.KeyName = os.environ['KEYNAME']
                if 'KUBECONFIG' in os.environ:
                    self.Kubeconfig = os.environ['KUBECONFIG']
            if self.UseCurrentNS:
                self.Namespaces = open("/var/run/secrets/kubernetes.io/serviceaccount/namespace").read()
        except Exception as error:
            raise ValueError('Error reading config file ' +
                             config_file+' ('+repr(error)+')')
        self.set_k8s_api()

    def set_k8s_api(self):
        logging.debug('Creating Kubernetes API connector')
        try:
            if self.InCluster:
                logging.debug('Loading config from the current cluster')
                config.load_incluster_config()
            else:
                logging.debug('Loading config from: {}'.format(self.Kubeconfig))
                config.load_kube_config(self.Kubeconfig)
        except IOError:
            try:
                config.load_incluster_config()  # How to set up the client from within a k8s pod
            except config.config_exception.ConfigException:
                raise ValueError("Could not configure kubernetes python client")

        self.api_instance = client.CoreV1Api()
        self.cmap = client.V1ConfigMap()

    def k8scm_write(self, data):
        # Actions to take to write data
        logging.debug('Writting')

        logging.debug('Creating CM definition')
        self.cmap.metadata = client.V1ObjectMeta(name=self.ConfigMapName)
        self.cmap.data = {}
        self.cmap.data[self.KeyName] = json.dumps(data, indent=4)

        logging.debug('Creating CM for each namespace')
        try:
            for n in self.Namespaces.split(','):
                logging.debug('namespace: {}'.format(n))
                logging.debug('Looking for configmap {}'.format(self.ConfigMapName))
                cms = self.api_instance.list_namespaced_config_map(namespace=n)
                exists = False
                body = None
                for cm in cms.items:
                    if cm.metadata.name == self.ConfigMapName:
                        logging.debug('Already exists, will need to compare it')
                        exists = True
                        body = cm.data
                        break
                if exists:
                    logging.debug('Comparing the actual and the propossed body for the CM')
                    if body != self.cmap.data:
                        logging.debug('Bodies are not equal, patching CM.')
                        self.api_instance.patch_namespaced_config_map(name=self.ConfigMapName, namespace=n, body=self.cmap)
                    else:
                        logging.debug('Bodies are equal, no further action required.')
                else:
                    logging.debug('Creating')
                    self.api_instance.create_namespaced_config_map(namespace=n, body=self.cmap)
        except Exception as error:
            logging.error('Can not create the CM: {}'.format(error))
            return False

        return True

    def k8scm_shutdown(self):
        # Actions to take on process shutdown
        return True


if __name__ == "__main__":
    quit()
