#!/usr/bin/env python

# external-dns SourceFile input generator
# This script will read from a given input and create the input file for external-dns' source SourceFile
# Author: Juan Matias KungFu de la Camara Beovide <juanmatias@gmail.com>
# Date: 2020-03-30

import logging
import time
import os
import io
import glob
import vendors.ConfigParser as ConfigParser
import pickle
import re

# This is the file reader plugin
# It reads a file from a set of paths
# These paths are set in config/input_file.conf


class file_reader:
    file_positions = {}
    regex_for_literal = re.compile('^\{.+\}$')

    def __init__(self):
        logging.debug("Reading config file for input_file module")
        try:
            config_file = os.path.dirname(os.path.realpath(
                __file__)) + '/../config/input_file.conf'
            self.configParser = ConfigParser.RawConfigParser()
            self.configParser.read(config_file)
            self.MaxBytes = int(self.configParser.get('Input', 'MaxBytes'))
            self.Method = self.configParser.get('Input', 'Method')
            try:
                self.RegEx = self.configParser.get('Input', 'RegEx')
            except:
                self.RegEx = ""
            try:
                self.Inverted = self.configParser.get('Input', 'Inverted')
            except:
                self.Inverted = "False"
            try:
                self.Separator = self.configParser.get('Input', 'Separator')
            except:
                self.Separator = ","
        except Exception as error:
            raise ValueError('Error reading config file ' +
                             config_file+' ('+repr(error)+')')

        logging.debug("Open registry file")
        try:
            with io.open(self.configParser.get('Registry', 'Path'), mode='rb', buffering=-1, encoding=None, errors=None, newline=None, closefd=True) as f:
                self.file_positions = pickle.load(f)
        except IOError:
            self.file_ack()
            with io.open(self.configParser.get('Registry', 'Path'), mode='rb', buffering=-1, encoding=None, errors=None, newline=None, closefd=True) as f:
                self.file_positions = pickle.load(f)
        except Exception as error:
            raise ValueError('Error opening registry file ' +
                             self.configParser.get('Registry', 'Path')+' ('+repr(error)+')')

    def file_read(self, data):
        # Actions to take to read data
        logging.debug("Reading data from sources")
        file_collection = []
        for f in self.configParser.get('Input', 'Paths').split(','):
            file_collection.extend(glob.glob(f))
        for f in file_collection:
            logging.debug("Reading data from "+f)
            try:
                if not f in self.file_positions:
                    self.file_positions[f] = {'offset': 0, 'inode': -1}
                logging.debug("Looking for inode")
                inode = os.stat(f).st_ino
                if self.file_positions[f]['inode'] != inode:
                    logging.debug('inode has changed, log file was rotated, must read from beginning (  '+str(self.file_positions[f]['inode'])+' vs. '+str(os.stat(f).st_ino)+')')
                    self.file_positions[f]['offset'] = 0
                with io.open(f, mode='r', buffering=-1, encoding=None, errors=None, newline=None, closefd=True) as fs:
                    if self.file_positions[f]['offset'] == 0 and self.MaxBytes != -1 and self.MaxBytes < os.fstat(fs.fileno()).st_size:
                        self.file_positions[f]['offset'] = os.fstat(fs.fileno()).st_size - self.MaxBytes
                        logging.debug("Resetting offset from 0 to {} - {} = {} ".format(os.fstat(fs.fileno()).st_size, self.MaxBytes,self.file_positions[f]['offset']))
                    logging.debug("Looking for offset "+str(self.file_positions[f]['offset']))
                    fs.seek(self.file_positions[f]['offset'])
                    localdata = []
                    logging.debug("Read Method: "+self.Method)
                    if self.Method == "RegEx":
                        try:
                            regex = re.compile(self.RegEx)
                        except Exception as error:
                            logging.error('Got an error handling regex for file reader: ' + repr(error))
                    for line in fs.readlines():
                        if self.Method == "Literal":
                            if self.regex_for_literal.match(line):
                                localdata.append(eval(line))
                            else:
                                logging.error('String does not match input regex: {}'.format(line))
                        elif self.Method == "RegEx":
                            try:
                                m = regex.match(line)
                                domain = ""
                                addresses = []
                                if m:
                                    if self.Inverted == "True":
                                        domain = m.group(2)
                                        addresses = m.group(1).split(self.Separator)
                                    else:
                                        domain = m.group(1)
                                        addresses = m.group(2).split(self.Separator)
                                    localdata.append({'dnsname': domain, 'addresses': addresses})
                                else:
                                    logging.error('No match with regex on line: ' + line)
                            except Exception as error:
                                logging.error('Got an error handling regex for file reader: ' + repr(error))
                    data.extend(localdata)
                    self.file_positions[f]['offset'] = fs.tell()
                    self.file_positions[f]['inode'] = inode
            except IOError:
                logging.debug('File '+f+' does not exist')
            except Exception as error:
                logging.warning('Error reading source '+f+' ('+repr(error)+')')

    def file_shutdown(self):
        # Actions to take on process shutdown
        return True

    def file_ack(self):
        # Actions to take when sent data ack is received
        logging.debug("Pickle file_positions to file")
        try:
            with io.open(self.configParser.get('Registry','Path'), mode='wb', buffering=-1, encoding=None, errors=None, newline=None, closefd=True) as f:
                pickle.dump(self.file_positions, f)
        except Exception as error:
            raise ValueError('Error piclking var ('+repr(error)+')')
        return True


if __name__ == "__main__":
    quit()
