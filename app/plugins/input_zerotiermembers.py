#!/usr/bin/env python

# external-dns SourceFile input generator
# This script will read from a given input and create the input file for external-dns' source SourceFile
# Author: Juan Matias KungFu de la Camara Beovide <juanmatias@gmail.com>
# Date: 2020-03-30

import logging
import vendors.ConfigParser as ConfigParser
import urllib.request
import json 
import os

# This is the reader plugin for ZeroTier members
# 
# Sample connection:
# curl -X GET https://my.zerotier.com/api/network/83eeeee632b7f0bb/member -H 'Authorization: bearer 7HeeeeeXHlYBp4EawLWO7NuAtP6WX1qs' | jq '.[] | .name, .config.ipAssignments'


class zerotiermembers_reader:
    BaseUrl = "https://my.zerotier.com/api/network/<networkid>"

    def __init__(self):
        logging.debug("Reading config file for input_zerotiermembers module")
        try:
            config_file = os.path.dirname(os.path.realpath(__file__)) + '/../config/input_zerotiermembers.conf'
            self.configParser = ConfigParser.RawConfigParser()
            self.configParser.read(config_file)
            self.NetworkID = self.configParser.get('Config', 'NetworkID')
            self.TokenMethod = self.configParser.get('Config', 'TokenMethod')
            if self.TokenMethod == 'Plain':
                self.Token = self.configParser.get('Config', 'Token')
            else:
                self.Token = ""
            try:
                self.BaseUrl = int(self.configParser.get('Config', 'BaseUrl'))
            except:
                pass
            self.BaseUrl = self.BaseUrl.replace('<networkid>', self.NetworkID)

        except Exception as error:
            raise ValueError('Error reading config file ' + config_file+' ('+repr(error)+')')

    def zerotiermembers_read(self, data):
        # Actions to take to read data

        # Read Data
        hdr = { 'Authorization': 'bearer ' + self.Token }
        url = self.BaseUrl+"/member"
        req = urllib.request.Request(url, headers=hdr)
        resps = ""
        logging.debug('Hitting url {}'.format(url))
        with urllib.request.urlopen(req) as url:
            resps = json.loads(url.read().decode())
        for resp in resps:
            data.append({'dnsname': resp["name"], 'addresses': resp["config"]["ipAssignments"]})

    def zerotiermembers_shutdown(self):
        # Actions to take on process shutdown
        return True

    def zerotiermembers_ack(self):
        # Actions to take when sent data ack is received
        return True


if __name__ == "__main__":
    quit()

