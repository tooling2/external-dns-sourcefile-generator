#!/usr/bin/env python3

# external-dns SourceFile input generator
# This script will read from a given input and create the input file for external-dns' source SourceFile
# Author: Juan Matias KungFu de la Camara Beovide <juanmatias@gmail.com>
# Date: 2020-03-30

import logging
import signal
import vendors.ConfigParser as ConfigParser
import os
import importlib
import time

from __version__ import _version
_appname = 'external-dns SourceFile Generator'
_appnametag = 'external-dns-sourcefile-generator'
_liveness = '/tmp/liveness'
_readiness = '/tmp/readiness'

def touch(fname, mode=0o666, dir_fd=None, **kwargs):
    flags = os.O_CREAT | os.O_APPEND
    with os.fdopen(os.open(fname, flags=flags, mode=mode, dir_fd=dir_fd)) as f:
        os.utime(f.fileno() if os.utime in os.supports_fd else fname,
            dir_fd=None if os.supports_fd else dir_fd, **kwargs)

class edsg_importer:
    def pimport(self, plugin, class_name):
        try:
            module = importlib.import_module("."+plugin, "plugins")
            class_ = getattr(module, class_name)
            instance = class_()
            return instance
        except Exception as error:
            raise ValueError('Can not import module ' + plugin+' ('+repr(error)+')')


class edsg_reader:
    def __init__(self, plugin):
        assert isinstance(plugin, str), 'Plugin name must be str'

        if(plugin == ''):
            logging.warning('Reader plugin can\'t be empty')
            raise ValueError('Reader plugin can\'t be empty')
        self.plugin = plugin
        logging.debug('Trying to load reader plugin: '+self.plugin)
        si = edsg_importer()
        try:
            self.r = si.pimport("input_"+self.plugin, self.plugin+"_reader")
        except Exception as error:
            logging.error('Got an error calling reader module: ' + repr(error))
            raise ValueError('Got an error calling reader module: ' + repr(error))

    def read(self, data):
        assert isinstance(data, list), 'Data must be a list'
        logging.debug('Reading data')
        try:
            getattr(self.r, self.plugin+"_read")(data)
        except Exception as error:
            logging.error('Got an error reading data: ' + repr(error))
            return False
        return True

    def stop(self):
        getattr(self.r, self.plugin+"_shutdown")()
        return True

    def ack(self):
        getattr(self.r, self.plugin+"_ack")()
        return True


class edsg_writer:
    def __init__(self, plugin):
        assert isinstance(plugin, str), 'Plugin must be str'
        if(plugin == ''):
            logging.warning('Writer plugin can\'t be empty')
            raise ValueError('Writer plugin can\'t be empty')
        self.plugin = plugin
        logging.debug('Trying to load writer plugin: '+self.plugin)
        si = edsg_importer()
        try:
            self.w = si.pimport('output_'+self.plugin, self.plugin+"_writer")
        except Exception as error:
            logging.error('Got an error calling writer module: ' + repr(error))
            raise ValueError('Got an error calling writer module: ' + repr(error))

    def write(self, data):
        assert isinstance(data, list), 'Data must be a list'

        logging.debug('writting data:')
        try:
            getattr(self.w, self.plugin+"_write")(data)
        except Exception as error:
            logging.error('Got an error calling module: ' + repr(error))
            return False
        logging.debug('flushing buffer')
        del data[:]
        return True

    def stop(self):
        getattr(self.w, self.plugin+"_shutdown")()
        return True


class eds_generator:
    interval_defaul = 5
    edsg_buffer = []

    def __init__(self):
        # Set log level
        logging.basicConfig(
            format='%(asctime)s %(levelname)s ( %(module)s ):%(message)s', level=logging.INFO)
        signal.signal(signal.SIGINT, self.stop)
        logging.info('{} version: {}'.format(_appname, _version))
        self.settings()

    def settings(self):
        logging.info('Reading config file for main process')
        self.configParser = ConfigParser.RawConfigParser()
        config_file = os.path.dirname(os.path.realpath(
            __file__)) + '/config/'+_appnametag+'.conf'
        self.configParser.read(config_file)
        self.WatchConf = bool(self.configParser.get('Config', 'WatchConf'))
        self.Kubernetes = bool(self.configParser.get('Config', 'Kubernetes'))
        if self.Kubernetes:
            touch(_liveness)
        self.reader_plugins = self.configParser.get('Plugins', 'Reader')
        self.writer_plugin = self.configParser.get('Plugins', 'Writer')
        if(isinstance(int(self.configParser.get('Config', 'Interval')), int)):
            logging.debug('Changing interval to ' +
                          self.configParser.get('Config', 'Interval'))
            self.interval_default = int(
                self.configParser.get('Config', 'Interval'))
        try:
            loglevel = self.configParser.get('Config', 'LogLevel')
            logging.info('Changing LogLevel to '+loglevel)
            print(eval("logging."+loglevel))
            for handler in logging.root.handlers[:]:
                logging.root.removeHandler(handler)
            logging.basicConfig(
                format='%(asctime)s %(levelname)s ( %(module)s ):%(message)s', level=eval("logging."+loglevel))
        except:
            pass

        logging.debug('Initializing objects')
        try:
            self.edsgr = []
            for reader_plugin in self.reader_plugins.split(','):
                self.edsgr.append(edsg_reader(reader_plugin))
            self.edsgw = edsg_writer(self.writer_plugin)
        except Exception as error:
            logging.error('Got an error creating plugins: ' + repr(error))
            self.stop(None, None)

    def start(self):
        logging.info('Starting loop.')

        if self.Kubernetes:
            touch(_readiness)

        logging.debug('Initializing loop')
        while True:
            if self.WatchConf:
                self.settings()
            logging.debug('Calling reader')
            for edsgr in self.edsgr:
                if not (edsgr.read(self.edsg_buffer)):
                    logging.error('Reader error')
                    self.stop(None, None)

            if(len(self.edsg_buffer) >= 0):
                logging.debug('Calling writer')
                if(self.edsgw.write(self.edsg_buffer)):
                    logging.debug('Logs processed')
                    if(edsgr.ack()):
                        logging.debug('Ack sent to reader')
                    else:
                        logging.error('Ack error')
                        self.stop(None, None)
                else:
                    logging.error('Writer error')
                    self.stop(None, None)
            else:
                logging.debug('No data to process')

            if(self.interval_default != -1):
                time.sleep(self.interval_default)
            else:
                self.stop(None, None)

    def stop(self, sig, frame):
        logging.info('Stopping loop.')
        if(sig != None):
            logging.debug('SIG {} received'.format(sig))
        logging.debug('Shutting down child processes')

        if self.Kubernetes:
            try:
                os.remove(_readiness)
            except OSError as e:  ## if failed, report it back to the user ##
                logging.error("Error: {} - {}.".format(e.filename, e.strerror))

        try:
            for edsgr in self.edsgr:
                edsgr.stop()
            self.edsgw.stop()
        except Exception as error:
            logging.error('Got an error shutting proccesses down: ' + repr(error))

        if self.Kubernetes:
            try:
                os.remove(_liveness)
            except OSError as e:  ## if failed, report it back to the user ##
                logging.error("Error: {} - {}.".format(e.filename, e.strerror))

        logging.debug('Shutting down main process')
        quit()


if __name__ == "__main__":
    edgs = eds_generator()
    edgs.start()
